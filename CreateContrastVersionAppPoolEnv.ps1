$versionTag = $OctopusParameters["Octopus.Release.Number"]
$appPool = $OctopusParameters["Octopus.Action.IISWebSite.ApplicationPoolName"]
Write-Output "APP Pool : " $appPool
Write-Output "Version Tag: " $versionTag
Try
{
    C:\windows\system32\inetsrv\appcmd.exe set config -section:system.applicationHost/applicationPools /-"[name='$appPool'].environmentVariables.[name='CONTRAST__APPLICATION__VERSION']" /commit:apphost
}
Catch
{
    Write-Output "Contrast environment variable did not exist continuing..."
}
Try
{
    C:\windows\system32\inetsrv\appcmd.exe set config -section:system.applicationHost/applicationPools /+"[name='$appPool'].environmentVariables.[name='CONTRAST__APPLICATION__VERSION',value='$versionTag']" /commit:apphost
}
Catch
{
    Write-Output "Unable to set Contrast environment variable continuing..."
}