$iisInfo = Get-ItemProperty HKLM:\SOFTWARE\Microsoft\InetStp\
Write-Output "IIS Info: " $iisInfo
$version = [decimal]"$($iisInfo.MajorVersion)"
Write-Output "IIS Version: " $version
$versionTag = $OctopusParameters["Octopus.Release.Number"]
Write-Output "Version Tag : " $versionTag
$appPool = $OctopusParameters["Octopus.Action.IISWebSite.ApplicationPoolName"]
Write-Output "Application Pool: " $appPool
$webSite = $OctopusParameters["Octopus.Action.IISWebSite.WebSiteName"]
Write-Output "Website Name: " $webSite
if( $version -ge 10)
{
    Write-Output "IIS version 10 setting the version Environment Variable for the ApplicationPool"
    Try
    {
        # Remove the CONTRAST__APPLICATION__VERSION environment variable if it exists
        C:\windows\system32\inetsrv\appcmd.exe set config -section:system.applicationHost/applicationPools /-"[name='$appPool'].environmentVariables.[name='CONTRAST__APPLICATION__VERSION']" /commit:apphost
    }
    Catch
    {
        Write-Output "Contrast environment variable did not exist continuing..."
    }
    Try
    {
        # Add the CONTRAST__APPLICATION__VERSION environment variable
        C:\windows\system32\inetsrv\appcmd.exe set config -section:system.applicationHost/applicationPools /+"[name='$appPool'].environmentVariables.[name='CONTRAST__APPLICATION__VERSION',value='$versionTag']" /commit:apphost
    }
    Catch
    {
        Write-Output "Unable to set Contrast environment variable continuing..."
    }
}
else
{
    Write-Output "IIS version is not 10, setting the version in web.config appSettings"
    Try
    {
        # Remove the contrast.application.version key in appSettings if it exists
        C:\windows\system32\inetsrv\appcmd.exe set config "$webSite" -section:appSettings /-"[key='contrast.application.version']"
    }
    Catch
    {
        Write-Output "Did not find contrast.applciation.version in <appSettings> continuing..."
    }
    Try
    {
        # Add the contrast.application.version key in appSettings
        C:\windows\system32\inetsrv\appcmd.exe set config "$webSite" -section:appSettings /+"[key='contrast.application.version',value='$versionTag']"
    }
    Catch
    {
        Write-Output "Unable to set contrast.applciation.version in appSettings continuing..."
    }
}


