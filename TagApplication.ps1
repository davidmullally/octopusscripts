# Setup TeamServer connection information

$contrastUrl ="https://app.contrastsecurity.com"     #ex: https://app.contrastsecurity.com
$orgId = "ENTER_ORG_ID_HERE"         # Organization Id - can get from Contrast url. Ex: 291225bb-fbbd-4e2d-b85c-56e2a60a6aae
$userId = "ENTER_USER_ID_HERE"         # TeamServerUserName in DotnetAgentSettings.ini file 
$serviceKey = "ENTER_SERVICE_KEY_HERE"                        # TeamServerServiceKey in DotnetAgentSettings.ini file
$apiKey = "ENTER_API_KEY_HERE"            # TeamServerApiKey in DotnetAgentSettings.ini file

# Create the Authorization header
$enc = [system.Text.Encoding]::ASCII
$authToken = [System.Convert]::ToBase64String($enc.GetBytes($userId + ":" + $serviceKey))

$appWc = New-Object System.Net.WebClient     
# Set the required headers for the API request
$appWc.Headers.Add("Authorization", $authToken)
$appWc.Headers.Add("API-Key", $apiKey)
$appWc.Headers.Add("Accept", "application/json")

# Get the application UUID based on the applciation name
# The filterText search matches any part of the application name so a unique name is needed here
# This example uses the Octopus project name as the application name
$appName = $OctopusParameters["Octopus.Project.Name"]
Write-Output "Searching for application with filterText: " $appName
$applicationUrl = "$contrastUrl/Contrast/api/ng/$orgId/applications?filterText=$appName"
$applicationResult = $appWc.DownloadString($applicationUrl)
$applicationData = $applicationResult | ConvertFrom-Json
$applicationUUID = $applicationData.applications.app_id
Write-Output "Got Application UUID: " $applicationUUID

# Check if we got just 1 string as the Application UUID 
# otherwise we had multiple applications returned from our filter
if ($applicationUUID -is [String]) {
    # Tag the application
    # Build the application tagging endpoint
    $tagUrl = "$contrastUrl/Contrast/api/ng/$orgId/tags/applications"
    # Get the tag to be used from the Octopus project
    $tag = $OctopusParameters["SHA256Tag"]
    Write-Output "Tagging application with Tag: " $tag

    $tagParams = @{"applications_id"=@("$applicationUUID");"tags"=@("$tag");}

    $tagWc = New-Object System.Net.WebClient     
    $tagWc.Headers.Add("Authorization", $authToken)
    $tagWc.Headers.Add("API-Key", $apiKey)
    $tagWc.Headers.Add("Accept", "application/json")
    $tagWc.Headers.Add("Content-Type", "application/json")

    Write-Output $tagParams | ConvertTo-Json
    $tagResult = $tagWc.UploadString($tagUrl, "PUT", ($tagParams|ConvertTo-Json))

    Write-Output $tagResult | ConvertFrom-Json

    $result = $tagResult | ConvertFrom-Json

    Write-Output $result.messages
}
